FROM golang:1.18

WORKDIR /app

COPY go.mod go.sum redis.conf ./

RUN go mod download

COPY *.go ./

RUN CGO_ENABLED=0 GOOS=linux go build -o /main

ENV PORT=3000
EXPOSE $PORT

CMD ["/main"]