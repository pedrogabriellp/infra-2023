package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
)

const (
	sessionDuration = time.Minute * 15
	adminUsername   = "admin"
	adminPassword   = "123456"
)

func createRoutes(app *fiber.App, redisClient *redis.Client) {
	app.Get("/", func(handler *fiber.Ctx) error {
		return handler.SendString("Hello, world!")
	})

	app.Post("/", func(handler *fiber.Ctx) error {
		username := handler.Query("username", "invalid")
		password := handler.Query("password", "invalid")

		if username != adminUsername {
			return handler.Status(fiber.StatusBadRequest).SendString("Invalid user")
		}

		if password != adminPassword {
			return handler.Status(fiber.StatusBadRequest).SendString("Invalid password")
		}

		session := uuid.New()

		status := redisClient.Set(context.Background(), session.String(), "admin", sessionDuration)
		if status.Err() != nil {
			log.Printf("[ERROR] - Error creating session: %s", status.Err())

			return handler.Status(fiber.StatusInternalServerError).
				SendString("Error creating sessions")
		}

		message := fmt.Sprintf("The new session is: %s", session)

		return handler.Status(fiber.StatusCreated).SendString(message)
	})

	app.Get("/exist/:session", func(handler *fiber.Ctx) error {
		session := handler.Params("session", "invalid")

		_, err := redisClient.Get(context.Background(), session).Result()
		if err != nil {
			if errors.Is(err, redis.Nil) {
				return handler.Status(fiber.StatusNotFound).SendString("Session does not exist")
			}

			log.Printf("[ERROR] - Error getting session: %s", err)

			return handler.Status(fiber.StatusInternalServerError).
				SendString("Error getting sessions")
		}

		return handler.SendString("The session exists")
	})
}

func main() {

	// Redis configuration:
	redisHost := fmt.Sprintf("%s", os.Getenv("REDIS_HOST"))
	redisPort := fmt.Sprintf("%s", os.Getenv("REDIS_PORT"))

	redisAddress := fmt.Sprintf("%s:%s", redisHost, redisPort)

	redisClient := redis.NewClient(&redis.Options{ //nolint:exhaustruct
		Addr:     redisAddress,
		Password: os.Getenv("REDIS_PASSWORD"),
		DB:       0,
	})

	// Route configuration:
	app := fiber.New()
	createRoutes(app, redisClient)

	port := fmt.Sprintf(":%s", os.Getenv("PORT"))

	err := app.Listen(port)
	if err != nil {
		log.Panicf("[ERROR] - Error listening on port %s: %v", port, err)
	}
}
